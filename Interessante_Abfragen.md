# Inhaltsverzeichnis

1. [Abfragen zum DBMS](#abfragen-zum-dbms)
   * [DBMS-Version](#dbms-version)
   * [Welche Datenbanken gibt es hier?](#welche-db)
   * [Welche Rollen (= Benutzer + Gruppen) gibt es hier?](#welche-rollen)
1. [Abfragen zu einer bestimmten Datenbank](#abfragen-zu-datenbank)
    * [Welche Tabellen existieren in dieser Datenbank?](#welche-tabellen)
    * [Beschreibung einer bestimmten Tabelle](#beschreibung-tabelle)
    * [Welche Constraints gibt es in der aktuellen Datenbank?](#welche-contraints)
    * [Wer hat von wem welche Berechtigungen auf der Tabelle bekommen?](#welche-berechtigungen-tabelle)

***

# <a name="abfragen-zum-dbms"></a> Abfragen zum DBMS
Hier finden sich Abfragen, die mit denen sich Informationen über das DBMS als ganzes gewinnen lassen.


***

## <a name="dbms-version"></a> DBMS-Version
Holt die Versionsinformation des DBMS, mit dem die aktuelle Session verbunden ist. 

[Quelle: Stackoverflow](https://stackoverflow.com/a/13733856)


```sql
SELECT version();
```
<details>
<summary>Beispielausgabe</summary>

```
PostgreSQL 13.1, compiled by Visual C++ build 1914, 64-bit
```
</details>


***


## <a name="welche-db"></a> Welche Datenbanken existieren in dem DBMS?
Holt Informationen zu den angelegten Datenbanken aus dem Systemkatalog. 
Die `template*`-Datenbanken sind die PostgreSQL-typischen Vorlagen für neue Datenbanken.
Die `postgres`-Datenbank ist enthält den Systemkatalog.

Äquivalent zum `SHOW DATABASES`-Befehl einiger DBMS.

[Quelle: PostgreSQL-Tutorial](https://www.postgresqltutorial.com/postgresql-show-databases/)


```sql
SELECT datname FROM pg_database;
```
<details>
<summary>Beispielausgabe</summary>

```
postgres
prg_sportfest
template1
template0
db_personen
```
</details>



***


## <a name="welche-rollen"></a> Welche Rollen (= Benutzer + Gruppen) gibt es hier?

PostgreSQL unterscheidet nicht zwischen "Benutzer" und "Gruppe". Stattdessen gibt es "Rollen" (roles), denen unterschiedliche Eigenschaften (Attribute) und Rechte (Privileges) zugewiesen werden können.
Die "Attribute" sind Eigenschaften die für diesen Datenbankcluster spezifisch sind, die "Rechte" sind SQL-Berechtigungen auf Datenbankobjekten.


Eine Rolle mit `LOGIN`-Attribut kann wie ein Benutzerkonto zur Anmeldung am DBMS genutzt werden. 
Eine Rolle _ohne_ `LOGIN`-Attribut funktioniert hingegen wie eine Gruppe: hier werden Attribute und Privileges gesammelt für alle Rollen freigegeben, die auch diese Rolle haben.

`psql` kennt das Metakommando `\du` beziehungsweise `\du+` ("describe user"), das ähnliche Funktion erfüllt.

[Quelle: Stack-Exchange: Database Administrators](https://dba.stackexchange.com/a/203229)

Die einzelnen Attribute sind in [der Dokumentation](https://www.postgresql.org/docs/13/sql-createrole.html) beschrieben.

```sql
SELECT role.rolname,
       role.rolsuper,
       role.rolcanlogin,
       role.rolinherit,
       role.rolcreaterole,
       role.rolcreatedb,
       role.rolconnlimit,
       role.rolvaliduntil,
       -- Aufzählung der Rollen, bei denen diese Rolle Mitglied ist
       ARRAY(SELECT b.rolname
             FROM pg_catalog.pg_auth_members m
                      INNER JOIN pg_catalog.pg_roles b ON m.roleid = b.oid
             WHERE m.member = role.oid) AS member_of,

       rolreplication,
       rolbypassrls -- RLS = Row-Level-Securtiy
FROM pg_catalog.pg_roles AS role
WHERE role.rolname !~ '^pg_'; -- Systemrollen ausblenden
```

<details>
<summary>Beispielausgabe (von einem Cluster, auf dem es nur die postgres-Rolle gibt)</summary>

| rolname | rolsuper | rolcanlogin | rolinherit | rolcreaterole | rolcreatedb | rolconnlimit | rolvaliduntil | member_of | rolreplication | rolbypassrls |
|---|---|---|---|---|---|---|---|---|---|---|
|postgres | true | true | true | true | true | -1 | null | {} | true | true |

</details>

***


# <a name="abfragen-zu-datenbank"></a> Abfragen zu einer bestimmten Datenbank
Hier finden sich Abfragen, mit denen sich Informationen über eine bestimmte Datenbank gewinnen lasssen.


***


## <a name="speicherplatz"></a> Benötigter Speicherplatz einer bestimmten Datenbank
Dies entspricht dem Plattenplatz, der RAM-Bedarf richtet sich nach der Konfiguration des DBMS und den Abfragen.

[Quelle: Stackoverflow](https://stackoverflow.com/a/14346397)


```sql
-- Datenbankname ergänzen
SELECT pg_size_pretty(pg_database_size('<Datenbankname>')) AS groesse;

-- am Beispiel 'db_personen'
SELECT pg_size_pretty(pg_database_size('db_personen')) AS groesse;
```
<details>
<summary>Beispielausgabe</summary>

```
8109 kB
```
</details>


***


## <a name="welche-tabellen"></a> Welche Tabellen existieren in dieser Datenbank?

Äquivalent zum `SHOW TABLES`-Befehl einiger DBMS.

[Quelle: PostgreSQL-Tutorial](https://www.postgresqltutorial.com/postgresql-show-tables/)

```sql
-- weitere mögliche Spalten: schemaname, tablename, tableowner, tablespace, hasindexes, hasrules, hastriggers, rowsecurity
SELECT tablename FROM pg_catalog.pg_tables WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema';
```

<details>
<summary>Beispielausgabe</summary>

| tablename |
|---|
| person | 
</details>


## <a name="beschreibung-tabelle"></a> Beschreibung einer bestimmten Tabelle

Beschreibung einer bestimmten Tabelle. 
`NOT-NULL`-Constraints auf Spalten sind hier sichtbar.
Äquivalent zum `DESCRIBE TABLE`-Befehl einiger DBMS.

[Quelle: PostgreSQL-Tutorial](https://www.postgresqltutorial.com/postgresql-describe-table/)

```sql
-- Tabellenname ergänzen
-- weitere mögliche Spalten: table_catalog, table_schema, table_name, column_name, ordinal_position, column_default, is_nullable, data_type, character_maximum_length, character_octet_length, numeric_precision, numeric_precision_radix, numeric_scale, datetime_precision, interval_type, interval_precision, character_set_catalog, character_set_schema, character_set_name, collation_catalog, collation_schema, collation_name, domain_catalog, domain_schema, domain_name, udt_catalog, udt_schema, udt_name, scope_catalog, scope_schema, scope_name, maximum_cardinality, dtd_identifier, is_self_referencing, is_identity, identity_generation, identity_start, identity_increment, identity_maximum, identity_minimum, identity_cycle, is_generated, generation_expression, is_updatable
-- dabei sind nicht alle Kombinationen für alle Spalten sinnvoll, identity*-Angaben sind nur bei IDENTITY-Spalten befüllt, character*-Angaben nur bei Text-/String-/Zeichen-Spalten.
SELECT column_name, data_type, is_identity, is_nullable
FROM information_schema.columns WHERE table_name = '<Tabellenname>';

-- Am Beispiel 'person'
SELECT column_name, data_type, is_identity, is_nullable
FROM information_schema.columns WHERE table_name = 'person';
```

<details>
<summary>Beispielausgabe</summary>

| column_name | data_type | is_identity | is_nullable |
|---|---|---|---|
| id | integer | YES | NO |
| vorname | text | NO | NO |
| nachname | text | NO | YES |
| email | text | NO | YES |
| geburtstag | date | NO | YES |
| geschlecht | character | NO | YES |

</details>


## <a name="welche-contraints"></a> Welche Constraints gibt es in der aktuellen Datenbank?

Auflistung der Constraints (ohne `NOT-NULL`-Constraints, die sind aus technischen Gründen hier nicht abbildbar).
Kann mittels `WHERE tabelle='Tabellenname'` auf eine bestimmte Tabelle beschränkt werden.

[Quelle: Stack-Exchange: Database Administrators](https://dba.stackexchange.com/a/214877)


```sql
SELECT conname, contype, relname -- Name, Typ (u = UNIQUE, c = CHECK, ...), Name der Tabelle
FROM pg_catalog.pg_constraint con 
INNER JOIN pg_catalog.pg_class rel ON rel.oid = con.conrelid;
```

<details>
<summary>Beispielausgabe</summary>

|  conname | contype | relname |
|---|---|---|
| email_laenger_3 | c | person |
| email_einzigartig | u | person |


</details>


***


## <a name="welche-berechtigungen-tabelle"></a> Wer hat von wem welche Berechtigungen auf der Tabelle bekommen?

Auflistung aller SQL-Berechtigungen mit Grantor ("von wem") und Grantee ("für wen") für eine bestimmte Tabelle.

[Quelle: Stack-Exchange: Database Administrators](https://dba.stackexchange.com/a/152566)

```sql
SELECT grantor,grantee, privilege_type
FROM information_schema.role_table_grants
WHERE table_name='<Tabellenname>';

-- am Beispiel 'planet':
SELECT grantor,grantee, privilege_type
FROM information_schema.role_table_grants
WHERE table_name='planet';
```

<details>
<summary>Beispielausgabe</summary>

|  grantor | grantee | privilege_type |
|---|---|---|
| postgres | postgres | INSERT |
| postgres | postgres | SELECT |
| postgres | postgres | UPDATE |
| postgres | postgres | DELETE |
| postgres | postgres | TRUNCATE |
| postgres | postgres | REFERENCES |
| postgres | postgres | TRIGGER |


</details>