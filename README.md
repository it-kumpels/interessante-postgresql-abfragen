# Interessante PostgreSQL-Abfragen

Eine Sammlung interessanter/nützlicher/wichtiger SQL-Abfragen für das PostgreSQL-DMBS.

Diese Sammlung entsteht im Rahmen des Datenbankunterrichts, da das Teams-Wiki (wo diese Informationen eigentlich hin gehören würden) leider keine Codeblöcke unterstützt.

Das "ganze Projekt" lebt im Moment in der Markdown-Datei `Interessante_Abfragen.md`, und so soll es möglichst auch bleiben.
Das hier ist eher ein "Notizzettel auf Speed" als ein Riesenprojekt.

# Wie man mitmachen kann

Eröffne ein Issue hier im [Issue-Tracker](https://gitlab.com/it-kumpels/interessante-postgresql-abfragen/-/issues).

Dein Issue sollte enthalten:
* die SQL-Abfrage in einer lauffähigen Form,
* eine Beschreibung, was die Abfrage liefert,
* möglichst eine Quellenangabe